var _Promise = require('babel-runtime/core-js/promise')['default'];

var pm2 = require('pm2');
var options = {
    id: process.argv[2],
    methodName: process.argv[3],
    data: process.argv[4]
};

module.exports = function connect(id, methodName, data) {
    return new _Promise(function (done, fail) {

        pm2.connect(function (err) {

            if (err) {
                console.error(err);
                process.exit(2);
            }

            pm2.start({
                name: "start",
                script: 'start.js'
            }, function (err, apps) {
                if (err) throw err;
            });

            pm2.start({
                name: "sender",
                script: 'sender.js', args: [id, methodName, data]
            }, function (err, apps) {
                if (err) throw err;

                pm2.launchBus(function (err, bus) {
                    bus.on('process:result', function (packet) {
                        if (packet.data.success) {
                            done();
                        }
                    });
                });
            });
        });
    });
};
//# sourceMappingURL=connect.js.map
