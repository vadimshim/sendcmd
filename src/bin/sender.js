/**
 * Created by Vadim on 21.10.2016.
 */
const pm2 = require('pm2');

import {PM2Sender, Command} from '../index';

console.log("SENDER STARTED");
var options = {
    id: process.argv[2],
    methodName: process.argv[3],
    data: process.argv[4]
};

async function Main(id, methodName, data) {
    var sender = new PM2Sender();
    try {

        var result = await Command(sender, id, methodName, data);
        if(result){
            process.send({
                type: 'process:result',
                data: {
                    success: true,
                    data: result
                }
            });
        }
        console.log(result);
    } catch (err) {
        console.log(err);
    }
};


Main(options.id, options.methodName, options.data);
