export class PM2Error {
    constructor(message, code, data = undefined) {
        this.message = message;
        this.code = code;
        this.data = data;
    }

    message;
    code;
    data;
}


export function ParseError(data = undefined) {
    return new PM2Error('Parse error.', -32700, data);
}

export function SystemError(data = undefined) {
    return new PM2Error('System error.', -32400, data ? {
        message: data.message,
        stack: data.stack
    } : null);
}

export function MethodNotFound(method = undefined) {
    return new PM2Error('Method not found.', -32601, method);
}

export function InvalidParams(param = undefined) {
    return new PM2Error('Invalid Params.', -32602, param);
}
export function ActionError(param = undefined) {
    return new PM2Error('Action Error.', -32666, param);
}