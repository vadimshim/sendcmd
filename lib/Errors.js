var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

exports.__esModule = true;
exports.ParseError = ParseError;
exports.SystemError = SystemError;
exports.MethodNotFound = MethodNotFound;
exports.InvalidParams = InvalidParams;
exports.ActionError = ActionError;

var PM2Error = function PM2Error(message, code) {
    var data = arguments.length <= 2 || arguments[2] === undefined ? undefined : arguments[2];

    _classCallCheck(this, PM2Error);

    this.message = message;
    this.code = code;
    this.data = data;
};

exports.PM2Error = PM2Error;

function ParseError() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new PM2Error('Parse error.', -32700, data);
}

function SystemError() {
    var data = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new PM2Error('System error.', -32400, data ? {
        message: data.message,
        stack: data.stack
    } : null);
}

function MethodNotFound() {
    var method = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new PM2Error('Method not found.', -32601, method);
}

function InvalidParams() {
    var param = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new PM2Error('Invalid Params.', -32602, param);
}

function ActionError() {
    var param = arguments.length <= 0 || arguments[0] === undefined ? undefined : arguments[0];

    return new PM2Error('Action Error.', -32666, param);
}
//# sourceMappingURL=Errors.js.map
