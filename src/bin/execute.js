#!/usr/bin/env node
const connect = require ('./connect');
const disconnect = require ('./disconnect');

var options = {
    id: process.argv[2],
    methodName: process.argv[3],
    data: process.argv[4]
};

async function execute (con, dis){
    try{
        await con(options.id, options.methodName, options.data);
        await dis();
    }catch(e){
        console.log(e);
    }
}

execute(connect, disconnect);