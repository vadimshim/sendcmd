var _Promise = require('babel-runtime/core-js/promise')['default'];

/**
 * Created by Vadim on 24.10.2016.
 */

var pm2 = require('pm2');

module.exports = function disconnect() {
    console.log("finish");
    return new _Promise(function (done, fail) {
        pm2['delete']('sender', function (err) {
            if (err) throw err;
        });

        pm2['delete']('start', function (err) {
            if (err) throw err;
            pm2.disconnect();
            done();
        });
    });
};
//# sourceMappingURL=disconnect.js.map
