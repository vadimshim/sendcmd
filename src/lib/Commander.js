import * as PM2Error from './Errors'

export default async function Command(sender, id, methodName, data) {
    try {
            var ID = parseInt(id);
            if (/\D/.test(ID)) {
                throw PM2Error.InvalidParams("id:", id)
            }
            if(methodName ==undefined)
            {
                throw PM2Error.InvalidParams("Command is missing");
            }
        return await sender.SendTask(ID, methodName, data);
    }
    catch (error) {
        var data = methodName+":"+error.data;
       throw PM2Error.ActionError(data);
    }
}

