var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _Promise = require('babel-runtime/core-js/promise')['default'];

exports.__esModule = true;
var pm2Bridge = require('pm2-bridge');
var pm2 = require('pm2');
var Errors = require('./Errors');

var PM2Sender = (function () {
    function PM2Sender() {
        _classCallCheck(this, PM2Sender);
    }

    PM2Sender.prototype.SendTask = _asyncToGenerator(function* (reciever, method, data) {
        console.log("SendTAsk");
        if (!data) data = null;
        try {
            var recieverInfo = yield this.GetRecieverInfo(reciever);
        } catch (err) {
            throw new Errors.SystemError(err);
        }
        return new _Promise(function (done, fail) {
            var request = {
                bridge: {
                    sender: {
                        pid: process.pid,
                        pm_id: process.env.pm_id,
                        name: process.env.name
                    },
                    reciever: recieverInfo
                },
                type: 'task',
                method: method,
                params: data,
                id: "XZ POKA"
            };
            console.log("REQUEST\n--------------\ndata", request, "\n------------");
            pm2Bridge.send({
                to: recieverInfo.name,
                data: request
            }).then(function (result) {
                if (result.type == 'result') {
                    done(result);
                }
                if (result.type == 'error') {
                    var method_not_found = new RegExp(request.method + " not found");

                    if (method_not_found.test(result.data.message)) {
                        fail(new Errors.MethodNotFound(request.method));
                    }
                    fail(result);
                }
            }, function (err) {
                console.log("big error", err);
                throw new Error('Reciever Error or Time is too long...');
            });
        });
    });

    PM2Sender.prototype.GetRecieverInfo = function GetRecieverInfo(id) {
        var _this = this;

        return new _Promise(function (done, fail) {
            pm2.connect(function () {
                pm2.list(function (err, list) {
                    var info;
                    try {
                        info = _this.findReciever(list, id);
                    } catch (err) {
                        fail(err);
                    }
                    done(info);
                });
            });
        });

        return info;
    };

    PM2Sender.prototype.findReciever = function findReciever(list, id) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].pm_id === id) {

                info = {
                    pid: list[i].pid,
                    pm_id: list[i].pm_id,
                    name: list[i].name
                };
                return info;
            }
        }
        throw new Error("GAGA");
    };

    return PM2Sender;
})();

exports['default'] = PM2Sender;
module.exports = exports['default'];
//# sourceMappingURL=PM2sender.js.map
