/**
 * Created by Vadim on 24.10.2016.
 */

const pm2 = require('pm2');

module.exports = function disconnect (){
    return new Promise((done, fail)=>{
        pm2.delete('sender', function (err) {
        if (err) throw err;
    });

    pm2.delete('start', function (err) {
        if (err) throw err;
        pm2.disconnect();
        done();
    });
    })
};
