#!/usr/bin/env node
var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var execute = _asyncToGenerator(function* (con, dis) {
    try {
        yield con(options.id, options.methodName, options.data);
        yield dis();
    } catch (e) {
        console.log(e);
    }
});

var connect = require('./connect');
var disconnect = require('./disconnect');
console.log("execute");

var options = {
    id: process.argv[2],
    methodName: process.argv[3],
    data: process.argv[4]
};

execute(connect, disconnect);
//# sourceMappingURL=execute.js.map
