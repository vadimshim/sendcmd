var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

exports.__esModule = true;

var _Errors = require('./Errors');

var _Errors2 = _interopRequireDefault(_Errors);

var pm2Bridge = require('pm2-bridge');

var PM2Reciever = (function () {
    function PM2Reciever(handlers) {
        _classCallCheck(this, PM2Reciever);

        this.handlers = handlers;
        this.init(this.handlers);
    }

    PM2Reciever.prototype.init = function init(handlers) {

        pm2Bridge.onMessage(_asyncToGenerator(function* (request) {
            try {
                if (handlers) //Проверяем наличие обработчиков
                    {
                        var methodResult;
                        for (var method in handlers) {
                            //Поиск нужного обработчика
                            if (method == request.data.method) {
                                methodResult = yield handlers[request.data.method](request.data.params); //запуск нужного обработчика
                            }
                        }
                        var response = { //подготовка шаблона ответа
                            bridge: {
                                reciever: {
                                    pid: process.pid,
                                    pm_id: process.env.pm_id,
                                    name: process.env.name
                                },
                                sender: request.data.bridge.sender
                            },
                            id: request.data.id
                        };
                        if (methodResult) {
                            // Если искомый обработчик был найден, передаем его результат
                            response.type = "result";
                            response.data = methodResult;
                            yield this.reply(response); // Отправляем ответ
                        } else {
                                // Если искомый обработчик не был найден, отправляем сообщение
                                response.type = "error";
                                response.data = { message: request.data.method + " not found" };
                                yield this.reply(response);
                            }
                    }
            } catch (ex) {
                //Обработка и отправка исключений
                var response = {
                    bridge: {
                        reciever: {
                            pid: process.pid,
                            pm_id: process.env.pm2_pid,
                            name: process.env.name
                        },
                        sender: request.data.bridge.sender
                    },
                    id: request.data.id,
                    type: "error",
                    data: ex.message
                };

                yield this.reply(response);
                console.log(ex);
            }
        }));
    };

    return PM2Reciever;
})();

exports['default'] = PM2Reciever;
module.exports = exports['default'];
//# sourceMappingURL=PM2reciever.js.map
