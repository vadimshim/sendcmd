const pm2Bridge = require('pm2-bridge');
const pm2 = require('pm2');
const Errors = require('./Errors');

export default class PM2Sender {
    constructor() {
    }

    async SendTask(reciever, method, data) {
        console.log("SendTAsk");
        if (!data) data = null;
        try {
            var recieverInfo = await this.GetRecieverInfo(reciever);
        }
        catch (err) {
            throw new Errors.SystemError(err);
        }
        return new Promise((done, fail)=> {
            var request = {
                bridge: {
                    sender: {
                        pid: process.pid,
                        pm_id: process.env.pm_id,
                        name: process.env.name
                    },
                    reciever: recieverInfo
                },
                type: 'task',
                method: method,
                params: data,
                id: "XZ POKA"
            }
            console.log("REQUEST\n--------------\ndata", request, "\n------------")
            pm2Bridge.send(
                {
                    to: recieverInfo.name,
                    data: request
                }
            ).then(function (result) {
                if (result.type == 'result') {
                    done(result);
                }
                if (result.type == 'error') {
                   var method_not_found = new RegExp(request.method+" not found")

                    if(method_not_found.test(result.data.message))
                    {
                        fail(new Errors.MethodNotFound(request.method));
                    }
                    fail(result);
                }
            }, function (err) {
                console.log("big error", err);
                throw new Error('Reciever Error or Time is too long...');
            });
        })
    }

    GetRecieverInfo(id) {
        return new Promise((done, fail)=> {
            pm2.connect(()=> {
                pm2.list((err, list)=> {
                    var info;
                    try {
                        info = this.findReciever(list, id);
                    }
                    catch (err) {
                        fail(err);
                    }
                    done(info);
                })
            });
        });


        return info;
    }

    findReciever(list, id) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].pm_id === id) {

                info = {
                    pid: list[i].pid,
                    pm_id: list[i].pm_id,
                    name: list[i].name
                }
                return info;

            }
        }
        throw new Error("GAGA");
    }

}


