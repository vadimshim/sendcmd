var _asyncToGenerator = require('babel-runtime/helpers/async-to-generator')['default'];

var Main = _asyncToGenerator(function* (id, methodName, data) {
    var sender = new _index.PM2Sender();
    try {

        var result = yield _index.Command(sender, id, methodName, data);
        if (result) {
            process.send({
                type: 'process:result',
                data: {
                    success: true,
                    data: result
                }
            });
        }
        console.log(result);
    } catch (err) {
        console.log(err);
    }
});

var _index = require('../index');

/**
 * Created by Vadim on 21.10.2016.
 */
var pm2 = require('pm2');

console.log("SENDER STARTED");
var options = {
    id: process.argv[2],
    methodName: process.argv[3],
    data: process.argv[4]
};

;

Main(options.id, options.methodName, options.data);
//# sourceMappingURL=sender.js.map
