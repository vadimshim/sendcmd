import PM2Error from './Errors'

const pm2Bridge = require('pm2-bridge');

export default class PM2Reciever {
    constructor(handlers){
        this.handlers = handlers;
        this.init(this.handlers);
    }

    init(handlers){

        pm2Bridge.onMessage( async function(request) {
            try
            {
                if( handlers ) //Проверяем наличие обработчиков
                {
                    var methodResult;
                    for ( var method in handlers){ //Поиск нужного обработчика
                        if(method == request.data.method){
                            methodResult = await handlers[request.data.method](request.data.params); //запуск нужного обработчика
                        }
                    }
                    var response = { //подготовка шаблона ответа
                        bridge: {
                                reciever: {
                                    pid: process.pid,
                                    pm_id: process.env.pm_id,
                                    name: process.env.name
                                },
                                sender: request.data.bridge.sender
                            },
                        id: request.data.id
                    }
                    if( methodResult ){ // Если искомый обработчик был найден, передаем его результат
                        response.type = "result";
                        response.data = methodResult;
                        await this.reply(response); // Отправляем ответ
                    }else{                          // Если искомый обработчик не был найден, отправляем сообщение
                        response.type = "error";
                        response.data = {message: (request.data.method+" not found")};
                        await this.reply(response);
                    }
                }
            }
            catch(ex){ //Обработка и отправка исключений
                var response = {
                    bridge: {
                        reciever: {
                            pid: process.pid,
                            pm_id: process.env.pm2_pid,
                            name: process.env.name
                        },
                        sender: request.data.bridge.sender
                    },
                    id: request.data.id,
                    type: "error",
                    data: ex.message
                }

                await this.reply(response);
                console.log(ex);
            }

        });

    }
}



