import {PM2Sender,Command} from '../index'
console.log("SENDER STARTED")
async function Main() {
    var sender = new PM2Sender();
    try {
        var result = await Command(sender);

        console.log(result);
    } catch (err) {
        console.log(err)
    }
}
Main();