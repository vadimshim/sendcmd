var _asyncToGenerator = require("babel-runtime/helpers/async-to-generator")["default"];

var _interopRequireWildcard = require("babel-runtime/helpers/interop-require-wildcard")["default"];

exports.__esModule = true;

var _Errors = require('./Errors');

var PM2Error = _interopRequireWildcard(_Errors);

exports["default"] = _asyncToGenerator(function* (sender, id, methodName, data) {
    try {
        var ID = parseInt(id);
        if (/\D/.test(ID)) {
            throw PM2Error.InvalidParams("id:", id);
        }
        if (methodName == undefined) {
            throw PM2Error.InvalidParams("Command is missing");
        }
        return yield sender.SendTask(ID, methodName, data);
    } catch (error) {
        var data = methodName + ":" + error.data;
        throw PM2Error.ActionError(data);
    }
});
module.exports = exports["default"];
//# sourceMappingURL=Commander.js.map
